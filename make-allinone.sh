#!/bin/bash
WORKDIR=${0%/*}
OUT=$WORKDIR/simplify_static_dir.pl

echo "Outputting to $OUT" >&2

shopt -s globstar
"$WORKDIR/util/perl-squasher/squash" \
    "$WORKDIR/simplify_static_dir-main.pl" \
    "$WORKDIR"/lib/**/*.pm \
    > "$OUT"
chmod +x -- "$OUT"
