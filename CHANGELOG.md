# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

### Added

- Add progress bar

### Changed

- Limit "Skipped" output to only summary
- Omit output of progress bar unless -v flag is present
- Add thousands separator commas to output

### Fixed

- Fixed issue where removable files wouldn't be linked with non-removable
  files.

## [3.0.0]

### Changed

- Re-order operations to make the linking happen at the very end

## [2.0.0]

### Fixed

- SHA1 collisions no longer corrupt yer data

### Changed

- Remove support for symlink generation

## [1.2.1]

- Fixed bug when processing files with \r characters in the name

## [1.2.0]

- Uses `Digest::SHA` instead of `Digest::MD5`

## [1.1.2]

- Added `-z` option
- Changed the default behavior to not process empty files

## [1.1.1]

- Added `-m` and `-M` options

## [1.1.0]

- Outputs GNU-style messages (ala `rm -v,' `ln -v,' etc.).
