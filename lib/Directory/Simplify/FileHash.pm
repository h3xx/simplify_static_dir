package Directory::Simplify::FileHash;
use strict;
use warnings;

=head1 DESCRIPTION

Object for abstracting management of a hashed filesystem

=cut

# :squash-remove-start:
require Directory::Simplify::File;
# :squash-remove-end:

sub new {
    my ($class, %args) = @_;
    return bless {
        _entries => {},
        _files_in_hash => {},
        %args,
    }, $class;
}

sub add {
    my ($self, %args) = @_;
    my @files = @{$args{files}};
    my $callback = $args{callback};
    foreach my $file (@files) {
        unless (ref $file eq 'Directory::Simplify::File') {
            $file = Directory::Simplify::File->new($file);
        }
        unless ($self->{_files_in_hash}->{$file->{name}}) {
            my $hash = $file->hash;

            unless (defined $self->{_entries}->{$hash}) {
                $self->{_entries}->{$hash} = [];
            }
            push @{$self->{_entries}->{$hash}}, $file;
            if (ref $callback eq 'CODE') {
                $callback->($file);
            }
        }
        $self->{_files_in_hash}->{$file->{name}} = 1;
    }
    return;
}

sub entries {
    my $self = shift;
    return values %{$self->{_entries}};
}

1;
