package Directory::Simplify::File;
use strict;
use warnings;
require Cwd;
use File::Basename qw/ dirname /;

sub new {
    my ($class, $rel_name) = @_;
    my $self = bless {
        rel_name => $rel_name,
        name => Cwd::abs_path($rel_name),
    }, $class;
    $self->{dirname} = dirname($self->{name});
    (@{$self}{qw/ dev ino mode nlink uid gid rdev size
                atime mtime ctime blksize blocks /})
        = lstat $self->{name};
    return $self;
}

sub hash {
    my $self = shift;
    unless (defined $self->{_hash}) {
        require Digest::SHA;
        my $ctx = Digest::SHA->new;
        $ctx->addfile($self->{name});
        $self->{_hash} = $ctx->hexdigest;
    }
    return $self->{_hash};
}

1;
