package Directory::Simplify::FileHash;
use strict;
use warnings;

=head1 DESCRIPTION

Object for abstracting management of a hashed filesystem

=cut

# :squash-ignore-start:
require Directory::Simplify::File;
# :squash-ignore-end:

sub new {
    my $class = shift;
    return bless {
        _entries => {},
        _files_in_hash => {},
        @_,
    }, $class;
}

sub add {
    my $self = shift;
    my (@files, $callback);
    if (ref $_[0] eq 'HASH') {
        # Called method like { files => [] }
        my %opts = %{$_[0]};
        @files = @{$opts{files}};
        $callback = $opts{callback};
    } else {
        @files = @_;
    }
    foreach my $file (@files) {
        unless (ref $file eq 'Directory::Simplify::File') {
            $file = Directory::Simplify::File->new($file);
        }
        unless ($self->{_files_in_hash}->{$file->{name}}) {
            my $hash = $file->hash;

            unless (defined $self->{_entries}->{$hash}) {
                $self->{_entries}->{$hash} = [];
            }
            push @{$self->{_entries}->{$hash}}, $file;
            &{$callback}($file) if ref $callback eq 'CODE';
        }
        $self->{_files_in_hash}->{$file->{name}} = 1;
    }
}

sub entries {
    my $self = shift;
    values %{$self->{_entries}}
}

1;
