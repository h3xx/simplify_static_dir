package Directory::Simplify::Utils;
# vi: et sts=4 sw=4 ts=4
use strict;
use warnings;

sub addcommas {
    my @added;
    foreach my $num (@_) {
        # don't split anything after the decimal
        my @parts = split /\./, $num;
        while ($parts[0] =~ s/(\d)(\d{3}(?:\D|$))/$1,$2/) {
        }
        push @added, (join '.', @parts);
    }
    wantarray ? @added : $added[0]
}

sub hr_size {
    my $sz = shift;
    my @sizes = qw/ B KB MB GB TB PB EB ZB YB /;
    my $fact = 1024;
    my $thresh = 0.1;
    my @ret;
    foreach my $exp (reverse 0 .. $#sizes) {
        if ($sz > (1 - $thresh) * $fact ** $exp) {
            @ret = ($sz / $fact ** $exp, $sizes[$exp]);
            last;
        }
    }

    # default to ($sz, 'bytes')
    @ret = ($sz, $sizes[0]) unless @ret;

    wantarray ? @ret : "@ret"
}

sub shell_quote {
    # shell-escape argument for inclusion in non-interpolated single quotes
    my @transformed = map {
        (my $out = $_)
            =~ s/'/'\\''/g;
        "'$out'";
    } @_;
    wantarray ? @transformed : $transformed[0];
}

1;
