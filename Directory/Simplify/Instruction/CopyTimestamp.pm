package Directory::Simplify::Instruction::CopyTimestamp;
# vi: et sts=4 sw=4 ts=4
use strict;
use warnings;
use overload '""' => 'as_string';

# :squash-ignore-start:
require Directory::Simplify::Utils;
# :squash-ignore-end:

sub new {
    my $class = shift;
    return bless {
        @_,
    }, $class;
}

sub run {
    my $self = shift;
    # preserve older time stamp
    utime $self->{source}->{atime}, $self->{source}->{mtime}, $self->{target}->{name};
}

sub bytes_freed {
    return 0;
}

sub as_string {
    my $self = shift;
    return sprintf 'touch -r %s %s', Directory::Simplify::Utils::shell_quote($self->{source}->{name}, $self->{target}->{name});
}

1;
