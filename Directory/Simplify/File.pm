package Directory::Simplify::File;
# vi: et sts=4 sw=4 ts=4
use strict;
use warnings;
require Cwd;

sub new {
    my $class = shift;
    my $rel_name = shift;
    my $self = bless {
        rel_name => $rel_name,
        name => Cwd::abs_path($rel_name),
    }, $class;
    (@{$self}{qw/ dev ino mode nlink uid gid rdev size
                atime mtime ctime blksize blocks /})
        = lstat $self->{name};
    $self
}

sub hash {
    my $self = shift;
    unless (defined $self->{_hash}) {
        require Digest::SHA;
        my $ctx = Digest::SHA->new;
        $ctx->addfile($self->{name});
        $self->{_hash} = $ctx->hexdigest;
    }
    $self->{_hash}
}

1;
