PROVE = prove -rv

TESTS_DIR = t

test:
	$(PROVE) -l -I$(TESTS_DIR) $(TESTS_DIR)

.PHONY: test
