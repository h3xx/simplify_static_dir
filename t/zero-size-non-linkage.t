#!perl
use strict;
use warnings;

use Test::More 'no_plan';

use TestFunctions;

my $tarball_dir = prep_tar();
my $test_dir = "$tarball_dir/t/zero-size";
my @files = (
    "$test_dir/empty1",
    "$test_dir/empty2",
);

# Smoke test
ok !are_hardlinked(@files), 'not hardlinked before we start';
run_script($test_dir);
ok file_exists(@files), 'files were not accidentally deleted';
ok !are_hardlinked(@files), 'zero-sized files did not get hardlinked';
