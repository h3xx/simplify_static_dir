#!perl
use strict;
use warnings;

use Test::More 'no_plan';

use TestFunctions;

my $tarball_dir = prep_tar();
my $test_dir = "$tarball_dir/t/timestamp-preservation";
my @files = (
    "$test_dir/newer-more-linked",
    "$test_dir/older-less-linked",
);

# Smoke test
ok !are_hardlinked(@files), 'not hardlinked before we start';
my $should_have_mtime = filemtime($files[1]);
run_script($test_dir);
ok file_exists(@files), 'files were not accidentally deleted';
ok are_hardlinked(@files), 'files should be hardlinked';
ok has_mtime($should_have_mtime, @files), 'timestamps updated to use oldest';
