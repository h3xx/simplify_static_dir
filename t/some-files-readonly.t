#!perl
use strict;
use warnings;

require Data::Dumper;
use Test::More 'no_plan';

use TestFunctions;

my $test_dir = mktempdir();

my %files = (
    rw1 => "$test_dir/rw-dir/1",
    rw2 => "$test_dir/rw-dir/2",
    ro1 => "$test_dir/ro-dir/ro-file",
);

PREP: {
    mkdir "$test_dir/ro-dir";
    mkdir "$test_dir/rw-dir";

    # Create two read-write links
    put_file($files{rw1});
    link $files{rw1}, $files{rw2};

    # Create a new less-linked but read-only file with the same contents
    put_file($files{ro1});

    # Lastly, make the directory read-only
    chmod 0555, "$test_dir/ro-dir";
}

my $ident_ro_before = gen_ident($files{ro1});
my $ident_rw_before = gen_ident($files{rw1});

my ($exit_code, $stdout, $stderr) = run_script_capture('-f', $test_dir);
is $exit_code, 0, 'script should not fail';

ok file_exists(values %files), 'files were not accidentally deleted';
is $ident_ro_before, gen_ident($files{ro1}), 'read-only file should not have been re-linked';
ok are_hardlinked(values %files), 'all files should become hard-linked ' . prettify_file_idents(values %files);

isnt $ident_rw_before, gen_ident($files{rw1}), 'the read-write file should become hard-linked';

sub put_file {
    my @files = @_;
    my $bytes = 1_048_576; # 1 MB
    foreach my $file (@files) {
        open my $fh, '>', $file
            or croak("Failed to open file $file for writing: $!");
        for (my $bytes_written = 0; $bytes_written < $bytes; ++$bytes_written) {
            print $fh 'A';
        }
        close $fh;
    }
    return;
}

sub prettify_file_idents {
    my @files = @_;
    my $d = Data::Dumper->new([{
        map { ($_, gen_ident($_)) } @files
    }]);
    $d->Indent(1);
    $d->Sortkeys(1);
    $d->Terse(1);
    return $d->Dump;
}
