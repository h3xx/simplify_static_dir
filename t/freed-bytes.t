#!perl
use strict;
use warnings;

use Test::More 'no_plan';

use TestFunctions;

my $tarball_dir = prep_tar();
my $test_dir = "$tarball_dir/t/freed-bytes";
my @files = (
    "$test_dir/1",
    "$test_dir/2",
    "$test_dir/3",
    "$test_dir/4",
);

# Smoke test
ok !are_hardlinked(@files), 'not hardlinked before we start';
my (undef, $stdout, $stderr) = run_script_capture('-f', $test_dir, $test_dir);
ok file_exists(@files), 'files were not accidentally deleted';
ok "freed 24 bytes (24 B)\n" eq $stderr, 'prints correct number of freed bytes';
