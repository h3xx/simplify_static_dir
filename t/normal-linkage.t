#!perl
use strict;
use warnings;

use Test::More 'no_plan';

use TestFunctions;

my $tarball_dir = prep_tar();
my $test_dir = "$tarball_dir/t/normal";
my @files = (
    "$test_dir/foo/same",
    "$test_dir/same",
);

# Smoke test
ok !are_hardlinked(@files), 'not hardlinked before we start';
run_script($test_dir);
ok file_exists(@files), 'files were not accidentally deleted';
ok are_hardlinked(@files), 'files with the same contents got hardlinked';
