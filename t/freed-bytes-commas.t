#!perl
use strict;
use warnings;

use Test::More 'no_plan';
use Carp qw/ croak /;

use TestFunctions;

my $test_dir = mktempdir();
put_file(
    "$test_dir/1",
    "$test_dir/2",
);

my (undef, $stdout, $stderr) = run_script_capture('-f', $test_dir);
ok "freed 1,048,576 bytes (1 MB)\n" eq $stderr, 'prints freed bytes with commas';

sub put_file {
    my @files = @_;
    my $bytes = 1_048_576; # 1 MB
    foreach my $file (@files) {
        open my $fh, '>', $file
            or croak("Failed to open file $file for writing: $!");
        for (my $bytes_written = 0; $bytes_written < $bytes; ++$bytes_written) {
            print $fh 'A';
        }
        close $fh;
    }
    return;
}
