# Static Dir Simplifier

Optimize directories for size by combining identical files using hard links.

## Options

Usage: `simplify_static_dir` **[OPTIONS]** **[DIR]...**

| Option | Meaning
| - | -
| -f                | Print a sum of the number of freed bytes.
| -m **REGEX**      | Only match file paths matching **REGEX**.
| -M **REGEX**      | Exclude file paths matching **REGEX**.
| -v                | Verbose output.
| -z                | Include zero-length files in search. Normally they are ignored (you don't save diskspace by hard-linking empty files).
| --help            | Output this help message and exit.
| --version         | Output version information and exit.

By default, scans the current directory.

## License

Copyright (C) 2010-2022 Dan Church.

License GPLv3: GNU GPL version 3.0 (https://www.gnu.org/licenses/gpl-3.0.html)

with Commons Clause 1.0 (https://commonsclause.com/).

This is free software: you are free to change and redistribute it.

There is NO WARRANTY, to the extent permitted by law.

You may NOT use this software for commercial purposes.
